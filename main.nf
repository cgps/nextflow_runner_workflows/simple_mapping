#!/usr/bin/env nextflow
/*

========================================================================================
              Simple reference-based mapping and phylogeny pipeline
========================================================================================
 Using primarily samtools/bcftools
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.0'

def versionMessage(){
  log.info"""
  ===========================================================
   Reference-based mapping and phylogeny version ${version}
  ===========================================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Mandatory arguments:
      --reference_sequence  The path to a fasta file the reference sequence to which the reads will be mapped
      --input_dir                 Path to input dir. This must be used in conjunction with fastq_pattern
      --fastq_pattern             The regular expression that will match fastq files e.g '*{R,_}{1,2}.fastq.gz'
      --output_dir                Path to output dir
      
    Optional arguments:
      --tree                      Whether to create a maximum likelihood tree
      --remove_recombination      Whether to remove recombination from the combined alignment using gubbins before
                                  producing the ML tree
      --filtering_conditions      The VCF filtering conditions to use. The default is:
                                  '%QUAL<25 || FORMAT/DP<10 || MAX(FORMAT/ADF)<5 || MAX(FORMAT/ADR)<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'
      --non_GATC_bases_threshold  The maximum fraction of non-GATC bases in a pseudogenome to accept. Those not passing will be written
                                  into a file <OUTPUT DIR>/pseudogenomes/low_quality_pseudogenomes.tsv. Default is 0.5
   """.stripIndent()
}

//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}
/***************** Setup inputs and channels ************************/
params.input_dir = false
params.fastq_pattern = false
params.output_dir = false
params.reference = false
params.tree = false
params.remove_recombination = false
params.filtering_conditions = false
params.non_GATC_bases_threshold = false

// setup fastq reads
input_dir = params.input_dir - ~/\/$/
fastqs = input_dir + '/' + params.fastq_pattern

// set up output directory
output_dir = params.output_dir - ~/\/$/

// set up input from reference sequnce
reference_sequence = file(params.reference)

// assign filtering parameters
if ( params.filtering_conditions ) {
  filtering_conditions = params.filtering_conditions
} else {
  filtering_conditions = '%QUAL<25 || FORMAT/DP<10 || MAX(FORMAT/ADF)<5 || MAX(FORMAT/ADR)<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'
}

// assign non GATC bases threshold
if ( params.non_GATC_bases_threshold ) {
  non_GATC_bases_threshold = params.non_GATC_bases_threshold
} else {
  non_GATC_bases_threshold = 0.5
}

log.info "======================================================================"
log.info "                    SNP calling pipeline"
log.info "======================================================================"
log.info "SNP pipeline version   : ${version}"
log.info "Fastq inputs      : ${fastqs}"
log.info "Reference                   : ${params.reference}"
log.info "======================================================================"
log.info "Outputs written to path     : ${params.output_dir}"
log.info "======================================================================"
log.info ""


// index reference sequence
process prepare_reference {
  tag {'prepare reference'}
  input:
  file ('reference.fasta') from reference_sequence

  output:
  file 'reference.*' into prepared_reference_files

  """
  bwa index reference.fasta
  """
}

// set up initial raw reads channel
Channel
  .fromFilePairs( fastqs )
  .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
  .set { raw_fastqs }


// run snp pipeline to create filtered vcfs //
// first map reads
process map_reads {
  tag { sample_id }

  publishDir "${output_dir}/sorted_bams",
    mode: 'copy'

  input:
  file ('reference.fasta') from reference_sequence
  file reference_file from prepared_reference_files
  set sample_id, file(reads) from raw_fastqs

  output:
  set sample_id, file("${sample_id}.sorted.bam") into sorted_bams

  script:
  """
  bwa mem reference.fasta ${reads[0]} ${reads[1]} | samtools view -bS -F 4 - | samtools sort -O bam -o ${sample_id}.sorted.bam -
  """
}

// call variants
process call_variants {
    tag { sample_id }

    input:
    file ('reference.fasta') from reference_sequence
    set sample_id, file(sorted_bam) from sorted_bams

    output:
    set sample_id, file("${sample_id}.bcf") into bcf_files

    script:
    """
    bcftools mpileup -a DP,AD,SP,ADF,ADR,INFO/AD,INFO/ADF,INFO/ADR -f reference.fasta ${sorted_bam} | bcftools call --ploidy 1 -m -Ob -o ${sample_id}.bcf
    """
}

// filter variants
process filter_variants {
    tag { sample_id }

    publishDir "${output_dir}/filtered_bcfs",
      mode: 'copy'

    input:
    set sample_id, file(bcf_file) from bcf_files

    output:
    set sample_id, file("${sample_id}.filtered.bcf") into filtered_bcf_files

    script:
    """
    bcftools filter -s LowQual -e'${filtering_conditions}' ${bcf_file} -Ob -o ${sample_id}.filtered.bcf

    """
}

// produce pseudogenome
process create_pseudogenome {
  tag { sample_id }

  publishDir "${output_dir}/pseudogenomes",
    mode: 'copy'

  input:
  file ('reference.fasta') from reference_sequence
  set sample_id, file(filtered_bcf_file) from filtered_bcf_files

  output:
  file("${sample_id}.fas") into pseudogenomes

  script:
  """
  filtered_bcf_to_fasta.py  -r reference.fasta -b ${filtered_bcf_file} -o ${sample_id}.fas
  """
}

process create_pseudogenome_alignment{
  tag { 'create pseudogenome alignment' }

  publishDir "${output_dir}/pseudogenomes",
    mode: 'copy'

  input:
  file ('reference.fasta') from reference_sequence
  file(pseudogenomes) from pseudogenomes.collect { it }

  output:
  file('aligned_pseudogenome.fas') into aligned_pseudogenome
  file('low_quality_pseudogenomes.tsv')

  script:
  """
  touch low_quality_pseudogenomes.tsv
  for pseudogenome in ${pseudogenomes}
  do
    fraction_non_GATC_bases=`calculate_fraction_of_non_GATC_bases.py -f \$pseudogenome | tr -d '\\n'`
    echo \$fraction_non_GATC_bases
    if [[ 1 -eq "\$(echo "\$fraction_non_GATC_bases < ${non_GATC_bases_threshold}" | bc)" ]]; then
      cat \$pseudogenome >> aligned_pseudogenome.fas
    else
      echo "\$pseudogenome\t\$fraction_non_GATC_bases" >> low_quality_pseudogenomes.tsv
    fi
  done
  cat reference.fasta >> aligned_pseudogenome.fas
  """

}


process create_variant_only_alignment{
  memory '15GB'
  cpus 4

  tag { 'create variant only pseudogenome alignment' }

  publishDir "${output_dir}/pseudogenomes",
    mode: 'copy'

  input:
  file('aligned_pseudogenome') from aligned_pseudogenome

  output:
  file('*.fas') into aligned_pseudogenome_variants_only

  script:
  if (params.remove_recombination){
    """
    run_gubbins.py --threads 4 -v -t hybrid aligned_pseudogenome
    snp-sites aligned_pseudogenome.filtered_polymorphic_sites.fasta -o aligned_pseudogenome.gubbins.variants_only.fas
    """

  } else {
    """
    snp-sites aligned_pseudogenome -o aligned_pseudogenome.variants_only.fas
    """
  }


}


  if (params.tree) {

  process build_tree {
    memory { 15.GB * task.attempt }
    cpus 4

    tag {'build tree'}
    publishDir "${output_dir}",
      mode: 'copy'

    input:
    file('aligned_pseudogenome.variants_only') from aligned_pseudogenome_variants_only

    output:
    file("*.treefile")
    file("*.contree")

    script:
    if (params.remove_recombination){
      """
      iqtree -s aligned_pseudogenome.variants_only -pre aligned_pseudogenome.gubbins.variants_only -m GTR+G -alrt 1000 -bb 1000 -nt AUTO -ntmax 4
      """
    } else {
      """
      iqtree -s aligned_pseudogenome.variants_only -m GTR+G -alrt 1000 -bb 1000 -nt AUTO -ntmax 4
      """
    }
  }
}
