FROM continuumio/miniconda3:4.6.14
LABEL authors="Anthony Underwood" \
      description="Docker image containing all requirements for assembly pipeline"

# Install bc
RUN apt update; apt install -y bc

# install dependencies via conda
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a

# add files
COPY bin/filtered_bcf_to_fasta.py /usr/local/bin/ 
RUN chmod 755 /usr/local/bin/filtered_bcf_to_fasta.py

COPY bin/calculate_fraction_of_non_GATC_bases.py /usr/local/bin/ 
RUN chmod 755 /usr/local/bin/calculate_fraction_of_non_GATC_bases.py

# point PATH at conda bin
ENV PATH /opt/conda/envs/mapping/bin:$PATH