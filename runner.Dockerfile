FROM nextflow/nextflow:19.07.0

RUN apk update && apk --no-cache add git 

RUN mkdir /pipelines && \
    git clone https://gitlab.com/cgps/nextflow_runner_workflows/simple_mapping.git /pipelines/simple_mapping && \
    chown -R nobody:nogroup /pipelines

WORKDIR /pipelines/simple_mapping
USER nobody

ENTRYPOINT [ "/usr/local/bin/nextflow" ]